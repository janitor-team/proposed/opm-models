Source: opm-models
Section: libs
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Arne Morten Kvarving <arne.morten.kvarving@sintef.no>, Markus Blatt <markus@dr-blatt.de>
Build-Depends: debhelper-compat (= 13), cmake (>=3.10),
 libtool, mpi-default-bin, mpi-default-dev, pkg-config,
 libdune-grid-dev (>= 2.6.0), libdune-istl-dev (>= 2.6.0),
 libdune-localfunctions-dev (>= 2.6.0), libopm-material-dev (>=2021.10),
 libboost-test-dev,
# Build-Depends-Indep: # broken on precise pbuilder
 doxygen, texlive-latex-extra, texlive-latex-recommended, ghostscript,
 zlib1g-dev
Standards-Version: 4.6.0
Homepage: http://opm-project.org
Vcs-Browser: https://salsa.debian.org/science-team/opm-models
Vcs-Git: https://salsa.debian.org/science-team/opm-models.git
Rules-Requires-Root: no

Package: libopm-models-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
 libdune-grid-dev (>= 2.6.0), libdune-istl-dev (>= 2.6.0),
 libdune-localfunctions-dev (>= 2.6.0), libopm-material-dev (>=2021.10)
Suggests: libopm-models-doc
Replaces: libopm-models1-dev
Description: C++ simulation framework for porous media flow -- development files
 The Open Porous Media (OPM) software suite provides libraries and
 tools for modeling and simulation of porous media processes, especially
 for simulating CO2 sequestration and improved and enhanced oil recovery.
 .
 opm-models is a header-only simulation framework which is primary focused
 on fully implicit models for flow and transport in porous media. It uses
 finite volume schemes for discretization and automatic differentiation
 for calculating the Jacobians. Its main objectives is to provide an
 easily usable, well maintainable, high performance framework which is
 capable of capturing all macro-scale scenarios relevant for academic
 research and industrial applications involving flow and transport
 processes in porous media.
 .
 This package provides the development files (headers) needed to build
 applications based on opm-models.

Package: libopm-models-doc
Architecture: all
Multi-Arch: foreign
Replaces: libopm-models1-doc
Section: doc
Depends: ${misc:Depends}
Description: C++ simulation framework for porous media flow -- documentation
 The Open Porous Media (OPM) software suite provides libraries and
 tools for modeling and simulation of porous media processes, especially
 for simulating CO2 sequestration and improved and enhanced oil recovery
 .
 opm-models is a header-only simulation framework which is primary focused
 on fully implicit models for flow and transport in porous media. It uses
 finite volume schemes for discretization and automatic differentiation
 for calculating the Jacobians. Its main objectives is to provide an
 easily usable, well maintainable, high performance framework which is
 capable of capturing all macro-scale scenarios relevant for academic
 research and industrial applications involving flow and transport
 processes in porous media.
 .
 opm-models is a simulation framework which is primary focused on fully implicit
 models for flow and transport in porous media. It uses finite volume schemes
 for discretization and automatic differentiation for calculating the Jacobians.
 Its main objectives is to provide an easily usable, well maintainable,
 high performance framework which is capable of capturing all macro-scale
 scenarios relevant for academic research and industrial applications
 involving flow and transport processes in porous media.
 .
 This package provides the documentation of the source code.
